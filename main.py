class Test:
    def __init__(self, b: str):
        self.a = b

    @staticmethod
    def app():
        return "a"

    @staticmethod
    def b():
        if 1 < 2 < 3 and 4 == (2 + 2):
            return 'b'
