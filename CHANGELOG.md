# Changelog

<!--next-version-placeholder-->

## v1.0.0 (2021-01-20)
### Feature
* Add app boot for loader ([`8cc7a39`](https://gitlab.com/frestrepo/test/-/commit/8cc7a39b807142b5b8be65187ac3d731164cad5a))
* Add main program ([`f3a8ce0`](https://gitlab.com/frestrepo/test/-/commit/f3a8ce092eedb9911270d862249b44679f924b6d))

### Fix
* Fix bug on main loader ([`9ed4ea6`](https://gitlab.com/frestrepo/test/-/commit/9ed4ea6ce7089bf9e1dad715a7d720012e2dc207))

### Breaking
* Should add new parameter on class instances  ([`c0c96b9`](https://gitlab.com/frestrepo/test/-/commit/c0c96b905120580acab89ec2539b223a741e33e5))

### Documentation
* Add readme file ([`dfe1967`](https://gitlab.com/frestrepo/test/-/commit/dfe1967aa29c6541785fe25840b5c19567c9afe1))

### Performance
* Adjust function to optimize memory ([`1e23ee7`](https://gitlab.com/frestrepo/test/-/commit/1e23ee7e5373f629367c1a5da331325f202a25d8))
